package com.cerotid.bank.io;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

import com.cerotid.bank.model.Customer;

public class ReadCustomers {

	public static void main(String[] args) {

		ReadCustomers obj = new ReadCustomers();

		String path = "customers.txt";
		char delimiter = ',';

		List<Customer> customerList = new ArrayList<>();
		obj.readFromCSVFile(path, delimiter).forEach(customerList::add);
		System.out.println(customerList);

		// get last name only:
		List<String> lastNames = new ArrayList<>();
		for (int i = 0; i < customerList.size(); i++) {
			String custLastName = customerList.get(i).getLastName();
			lastNames.add(custLastName);
		}

		obj.writeToCSVFile("output.txt", lastNames);

	}

	public List<Customer> readFromCSVFile(String path, char delimiter) {

		FileReader fR = null;
		try {
			fR = new FileReader(path);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		List<Customer> customers = new ArrayList<>();
		BufferedReader bR = new BufferedReader(fR);
		try {
			String line = bR.readLine();
			while ((line = bR.readLine()) != null && !line.isEmpty()) {
				StringTokenizer st = new StringTokenizer(line, ",");
				String lastName = st.nextToken();
				String firstName = st.nextToken();

				Customer obj = new Customer();
				obj.setLastName(lastName);
				obj.setFirstName(firstName);

				customers.add(obj);

			}
			Collections.sort(customers, Customer.lastNameComparator);

		} catch (IOException e) {
			e.printStackTrace();
		}

		return customers;
	}

	public void writeToCSVFile(String path, List<String> customers) {
		try {
			FileWriter csvWriter = new FileWriter(path);
			csvWriter.append("Last Name");
			csvWriter.append("\n");

			for (String rowData : customers) {
				csvWriter.append(String.join(",", rowData));
				csvWriter.append("\n");
			}

			csvWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
