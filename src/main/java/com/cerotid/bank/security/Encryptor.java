package com.cerotid.bank.security;

import java.util.Base64;

public class Encryptor {

	public static String getEncodedString(String password) {
		return Base64.getEncoder().encodeToString(password.getBytes());
	}

}
