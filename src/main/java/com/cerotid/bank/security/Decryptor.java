package com.cerotid.bank.security;

import java.util.Base64;

public class Decryptor {
	public static String getDecryptedString(String encryptedString) {
		return new String(Base64.getMimeDecoder().decode(encryptedString));
	}
}
