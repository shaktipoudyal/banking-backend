package com.cerotid.bank.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cerotid.bank.model.AuthenticationRequest;
import com.cerotid.bank.model.AuthenticationResponse;
import com.cerotid.bank.model.User;
import com.cerotid.bank.service.BankBoImplement;
import com.cerotid.bank.springsecurity.JwtUtil;
import com.cerotid.bank.springsecurity.MyUserDetailsService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/userlogin")
public class UserLoginController {

	@Autowired
	BankBoImplement bankImpl;

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	private MyUserDetailsService userDetailsService;

	@Autowired
	private JwtUtil jwtTokenUtil;

	@PostMapping("/authenticate")
	public ResponseEntity<?> createAuthticationToken(@RequestBody AuthenticationRequest authenticationRequest)
			throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
					authenticationRequest.getUsername(), authenticationRequest.getPassword()));
		} catch (BadCredentialsException e) {
			throw new Exception("Incorrect credentials!", e);
		}

		final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
		final String jwt = jwtTokenUtil.generateToken(userDetails);

		return ResponseEntity.ok(new AuthenticationResponse(jwt));
	}

	@PostMapping("/register")
//	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<?> registerUser(@RequestBody User user) throws Exception {
		System.out.println(user.toString());

		userDetailsService.register(user);

		return ResponseEntity.ok().build();
	}

	@PostMapping("/checkAvailability")
	public ResponseEntity<?> usernameAvailability(@RequestBody User user) throws Exception {
		System.out.println(user.toString());
		userDetailsService.usernameAvailability(user);
		return ResponseEntity.ok().build();
	}

	@PostMapping("/logout")
	public ResponseEntity<?> logout() throws Exception {
		userDetailsService.logout();
		return ResponseEntity.ok().build();
	}
}