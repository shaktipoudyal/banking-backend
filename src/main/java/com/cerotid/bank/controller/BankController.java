package com.cerotid.bank.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.cerotid.bank.common.State;
import com.cerotid.bank.model.Account;
import com.cerotid.bank.model.AccountType;
import com.cerotid.bank.model.Address;
import com.cerotid.bank.model.Customer;
import com.cerotid.bank.service.BankBoImplement;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class BankController {

	@Autowired
	BankBoImplement bankImpl;

	@GetMapping("/{form}")
	public String showAccountForm(Model model, @PathVariable("form") String form) {
		switch (form) {
		case "register":
		case "account":
		case "customerselection":
			Customer customer = new Customer();
			model.addAttribute("customer", customer);
			model.addAttribute("stateCode", new ArrayList<State>(Arrays.asList(State.values())));
			model.addAttribute("accountType", new ArrayList<AccountType>(Arrays.asList(AccountType.values())));

			List<Customer> customers = new ArrayList<>();
			bankImpl.getCustomers().forEach(customers::add);

			model.addAttribute("accounts", customers);

			break;
		}
		return form;
	}

	@PostMapping("/registerCustomer")
//	public void registerCustomer(@Valid @ModelAttribute("customer") Customer customer, BindingResult result)
	public void registerCustomer(@RequestBody Customer customer, BindingResult result) throws Exception {
		System.out.println("---register is called---");
		System.out.println(customer.toString());
		if (result.hasErrors()) {
			ModelAndView model1 = new ModelAndView("register");
		}

		System.out.println("contoller: " + customer.toString());

		bankImpl.addCustomer(customer);
	}

	@PostMapping("/selectedcustomer")
	@ResponseBody
	public void selectedCustomer(@Valid @ModelAttribute("customer") Customer customer, BindingResult result)
			throws Exception {

		System.out.println("contoller: " + customer.toString());

//		showAccountForm(customer,  "customerselection");
	}

	// @ModelAttribute("state") State state,
	@GetMapping("/getCustomers/{code}")
	@ResponseBody
	public List<Customer> getCustomersByState(@PathVariable("code") State code) throws Exception {
		String stateCode = code.name();
		return bankImpl.getCustomersByState(stateCode);
	}

	@GetMapping("/getCustomers")
	public List<Customer> getCustomers() throws Exception {

		return bankImpl.getCustomers();
	}

	@PutMapping("/api/updateCustomers")
	@ResponseBody
	public void update(@ModelAttribute("customer") Customer customer, HttpServletRequest request) throws Exception {
		// fetch address information-->
		Address address = new Address();
		String state = request.getParameter("stateCode");
		State stateCode = Enum.valueOf(State.class, state);
		String streetName = request.getParameter("streetName");
		String city = request.getParameter("city");
		String zipCode = request.getParameter("zipCode");
		System.out.println(zipCode + " " + city);

		address.setStateCode(stateCode);
		address.setStreetName(streetName);
		address.setCity(city);
		address.setZipCode(zipCode);

		System.out.println(address.toString());
		customer.setAddress(address);

		bankImpl.editCustomerInfo(customer);
	}

	@GetMapping("/getCustomerBySSN/{ssn}")
	public Customer getCustomerBySsn(@PathVariable("ssn") String ssn) throws Exception {
		return bankImpl.getCustomerInfo(ssn);
	}

	@PostMapping("/openAccount")
	public void openAccount(@RequestBody Customer customer) {
		System.out.println("openAccount called");
		System.out.println(customer.toString());
		bankImpl.openCustomerAccount(customer);
	}

	@GetMapping("/getByAccountNumber/{accountNumber}")
	public Account getByAccountNumber(@PathVariable("accountNumber") long accountNumber) {
		return bankImpl.findByAccountNumber(accountNumber);
	}

	/*
	 * for test only
	 */
	@GetMapping("/getAccounts")
	@ResponseBody
	public List<Account> getAccounts() {
		System.out.println("getAccounts called");
		return bankImpl.getAccounts();
	}

	@DeleteMapping("/deleteCustomer/{id}")
	public void deleteCustomer(@PathVariable("id") int id) throws Exception {
		bankImpl.deleteCustomer(id);
	}

	@PostMapping("/deposit/{accountNumber}/{depositAmount}")
	public void deposit(@PathVariable("accountNumber") long accountNumber,
			@PathVariable("depositAmount") double depositAmount) throws Exception {
		bankImpl.deposit(accountNumber, depositAmount);
	}

	@PostMapping("/withdraw/{accountNumber}/{withdrawAmount}")
	public void withdraw(@PathVariable("accountNumber") long accountNumber,
			@PathVariable("withdrawAmount") double withdrawAmount) throws Exception {
		bankImpl.withdraw(accountNumber, withdrawAmount);
	}
}