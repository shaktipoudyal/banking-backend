package com.cerotid.bank.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.cerotid.bank.common.State;

@Entity
public class Address {
	int id;
	String streetName;
	String zipCode;
	String city;
	State stateCode;

	public Address() {

	}

	public Address(int id, String streetName, String zipCode, String city, State stateCode) {
		super();
		this.id = id;
		this.streetName = streetName;
		this.zipCode = zipCode;
		this.city = city;
		this.stateCode = stateCode;
	}

	@Id
	@GeneratedValue
	@Column(name = "id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Enumerated(EnumType.STRING)
	public State getStateCode() {
		return stateCode;
	}

	public void setStateCode(State stateCode) {
		this.stateCode = stateCode;
	}

	@Override
	public String toString() {
		return "Address [streetName=" + streetName + ", zipCode=" + zipCode + ", city=" + city + ", stateCode="
				+ stateCode + "]";
	}

//	In Address - streeName: String; zipCode: String; city: String; StateCode: String
}
