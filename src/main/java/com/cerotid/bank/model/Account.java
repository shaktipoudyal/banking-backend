package com.cerotid.bank.model;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Account {
	int id;

	@Enumerated(EnumType.STRING)
	AccountType accountType;

	Date accountOpenDate;
	Date accountCloseDate;
	double amount;
	UUID accountNumber = UUID.randomUUID();
	long bankAccNumber;
	int customerId;

	public Account() {

	}

	public Account(long bankAccNumber, AccountType accountType, Date accountOpenDate, Date accountCloseDate,
			double amount, int customerId) {
		super();
		this.bankAccNumber = bankAccNumber;
		this.accountType = accountType;
		this.accountOpenDate = accountOpenDate;
		this.accountCloseDate = accountCloseDate;
		this.amount = amount;
		this.customerId = customerId;
	}

	public Account(AccountType accountType, double amount) {
		super();
		this.accountType = accountType;
		this.amount = amount;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Enumerated(EnumType.STRING)
	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	public Date getAccountOpenDate() {
		return accountOpenDate;
	}

	public void setAccountOpenDate(Date accountOpenDate) {
		this.accountOpenDate = accountOpenDate;
	}

	public Date getAccountCloseDate() {
		return accountCloseDate;
	}

	public void setAccountCloseDate(Date accountCloseDate) {
		this.accountCloseDate = accountCloseDate;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public UUID getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(UUID accountNumber) {
		this.accountNumber = accountNumber;
	}

//	@ManyToOne()
//	@JoinColumn(name = "cust_id")
//	public Customer getCustomer() {
//		return customer;
//	}
//
//	public void setCustomer(Customer customer) {
//		this.customer = customer;
//	}

	public long getBankAccNumber() {
		return bankAccNumber;
	}

	public void setBankAccNumber(long bankAccNumber) {
		this.bankAccNumber = bankAccNumber;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	@Override
	public String toString() {
		return "Account [id=" + id + ", accountType=" + accountType + ", accountOpenDate=" + accountOpenDate
				+ ", accountCloseDate=" + accountCloseDate + ", amount=" + amount + ", accountNumber=" + accountNumber
				+ ", bankAccNumber=" + bankAccNumber + ", customerId=" + customerId + "]";
	}

}
