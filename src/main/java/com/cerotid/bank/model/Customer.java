package com.cerotid.bank.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "customer")
public class Customer implements Comparator<Customer> {

	int id;
	@NotNull
	String firstName;
	@NotNull
	String lastName;
	@NotNull
	String ssn;
//	@NotNull
	Address address;

	List<Account> accounts = new ArrayList<>();

	public Customer() {

	}

	public Customer(int id, @NotNull String firstName, @NotNull String lastName, @NotNull String ssn) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.ssn = ssn;
	}

	public Customer(int id, String firstName, String lastName, String ssn, Address address) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.ssn = ssn;
		this.address = address;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "addressId_FK")
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

//	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//	@JoinColumn(name = "acc_fk")
	@ElementCollection(targetClass=Account.class)
	public List<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}

	@Override
	public String toString() {
		return "Customer [customerId=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", ssn=" + ssn
				+ ", address=" + address + ", accounts=" + accounts + "]";
	}

	public static Comparator<Customer> lastNameComparator = new Comparator<Customer>() {
		@Override
		public int compare(Customer o1, Customer o2) {
			return o1.lastName.compareTo(o2.lastName);
		}
	};

	@Override
	public int compare(Customer o1, Customer o2) {
		return o1.firstName.compareTo(o2.firstName);
	}
}
//	In Customer - firstName: String; lastName: String; accounts : ArrayList <Account>; address: Address
//	Behavior - printCustomerAccounts
//	           printCustomerDetails (Example: Name, Accounts, Address)
//}
