package com.cerotid.bank.model;

public class Transaction {

	double amount;
	double fee;
	String receiverFirstName;
	String receiverLastName;

	public Transaction() {

	}

	public Transaction(double amount, double fee, String receiverFirstName, String receiverLastName) {
		super();
		this.amount = amount;
		this.fee = fee;
		this.receiverFirstName = receiverFirstName;
		this.receiverLastName = receiverLastName;
	}

	// deductAccountBalance()

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getFee() {
		return fee;
	}

	public void setFee(double fee) {
		this.fee = fee;
	}

	public String getReceiverFirstName() {
		return receiverFirstName;
	}

	public void setReceiverFirstName(String receiverFirstName) {
		this.receiverFirstName = receiverFirstName;
	}

	public String getReceiverLastName() {
		return receiverLastName;
	}

	public void setReceiverLastName(String receiverLastName) {
		this.receiverLastName = receiverLastName;
	}

	@Override
	public String toString() {
		return "Transaction [amount=" + amount + ", fee=" + fee + ", receiverFirstName=" + receiverFirstName
				+ ", receiverLastName=" + receiverLastName + "]";
	}

}
