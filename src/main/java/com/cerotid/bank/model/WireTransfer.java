package com.cerotid.bank.model;

public class WireTransfer extends Transaction {

	String beneficiaryFirstName;
	String beneficiaryLastName;
	String intermediaryBankSWIFTCode;
	String beneficiaryBankName;
	String beneficiaryAccountNumber;

	public WireTransfer() {

	}

	public WireTransfer(String beneficiaryFirstName, String beneficiaryLastName, String intermediaryBankSWIFTCode,
			String beneficiaryBankName, String beneficiaryAccountNumber) {
		super();
		this.beneficiaryFirstName = beneficiaryFirstName;
		this.beneficiaryLastName = beneficiaryLastName;
		this.intermediaryBankSWIFTCode = intermediaryBankSWIFTCode;
		this.beneficiaryBankName = beneficiaryBankName;
		this.beneficiaryAccountNumber = beneficiaryAccountNumber;
	}

	public String getBeneficiaryFirstName() {
		return beneficiaryFirstName;
	}

	public void setBeneficiaryFirstName(String beneficiaryFirstName) {
		this.beneficiaryFirstName = beneficiaryFirstName;
	}

	public String getBeneficiaryLastName() {
		return beneficiaryLastName;
	}

	public void setBeneficiaryLastName(String beneficiaryLastName) {
		this.beneficiaryLastName = beneficiaryLastName;
	}

	public String getIntermediaryBankSWIFTCode() {
		return intermediaryBankSWIFTCode;
	}

	public void setIntermediaryBankSWIFTCode(String intermediaryBankSWIFTCode) {
		this.intermediaryBankSWIFTCode = intermediaryBankSWIFTCode;
	}

	public String getBeneficiaryBankName() {
		return beneficiaryBankName;
	}

	public void setBeneficiaryBankName(String beneficiaryBankName) {
		this.beneficiaryBankName = beneficiaryBankName;
	}

	public String getBeneficiaryAccountNumber() {
		return beneficiaryAccountNumber;
	}

	public void setBeneficiaryAccountNumber(String beneficiaryAccountNumber) {
		this.beneficiaryAccountNumber = beneficiaryAccountNumber;
	}

	@Override
	public String toString() {
		return "WireTransfer [beneficiaryFirstName=" + beneficiaryFirstName + ", beneficiaryLastName="
				+ beneficiaryLastName + ", intermediaryBankSWIFTCode=" + intermediaryBankSWIFTCode
				+ ", beneficiaryBankName=" + beneficiaryBankName + ", beneficiaryAccountNumber="
				+ beneficiaryAccountNumber + "]";
	}
}
