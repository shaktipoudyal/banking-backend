package com.cerotid.bank.model;

import java.util.ArrayList;

public class Bank {

	private String bankName;
	private ArrayList<Customer> customers;

	public Bank() {

	}

	public Bank(String bankName, ArrayList<Customer> customers) {
		super();
		this.bankName = bankName;
		this.customers = customers;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public ArrayList<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}

//	In Bank - Member variables are - bankName: String; customers: ArrayList<Customer>
//Behavior - printBankName
//           printBankDetails (BankName with associated Customers)

}
