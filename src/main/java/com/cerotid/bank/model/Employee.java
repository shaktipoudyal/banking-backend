package com.cerotid.bank.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int employeeId;
	String name;
	int ssn;
	double salary;

	public Employee() {

	}

	public Employee(int employeeId, String name, int ssn, double salary) {
		super();
		this.employeeId = employeeId;
		this.name = name;
		this.ssn = ssn;
		this.salary = salary;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSsn() {
		return ssn;
	}

	public void setSsn(int ssn) {
		this.ssn = ssn;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Employee [employeeId=" + employeeId + ", name=" + name + ", ssn=" + ssn + ", salary=" + salary + "]";
	}

//	printEmployeeInfo, printSocialSecurityandName
//
//	Main method= Create 2 employee objects
//	Use printEmployeeInfo and printSocialSecurityandName
//
//	2. ClassName = StarPrinter
//
//	   *
//	   **
//	   ***
//	   ****
//	   *****
//
//	3. ClassName = PrimeNumberFinder
//	Print prime number from 1 to 100.
//	Hit: Use %
//
//	Example:  Prime number from 2-10 are 2, 3, 5, 7
//
//	2, 3, 5, 7

}
