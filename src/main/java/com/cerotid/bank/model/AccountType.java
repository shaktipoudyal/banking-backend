package com.cerotid.bank.model;

public enum AccountType {
	Checking, Saving, Business_Checking;

	AccountType() {

	}

	private String type;

	AccountType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

}
