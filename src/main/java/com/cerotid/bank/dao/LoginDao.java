package com.cerotid.bank.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cerotid.bank.model.Login;

public interface LoginDao extends JpaRepository<Login, Integer> {
	@Query(value = "Select * from login l where l.username=:username and l.password=:password", nativeQuery = true)
	public Login findByUsernameAndPassword(@Param("username") String username, @Param("password") String password);

}
