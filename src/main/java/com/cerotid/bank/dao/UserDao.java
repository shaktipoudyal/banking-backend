package com.cerotid.bank.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cerotid.bank.model.User;

@Repository
public interface UserDao extends JpaRepository<User, Integer> {

	@Query(value = "select * from user_entity u where u.user_name=:user_name", nativeQuery = true)
	public User findByUserName(@Param("user_name") String user_name);

}
