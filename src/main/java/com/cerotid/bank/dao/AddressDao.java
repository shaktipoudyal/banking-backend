package com.cerotid.bank.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cerotid.bank.common.State;
import com.cerotid.bank.model.Address;

public interface AddressDao extends JpaRepository<Address, Integer> {
	@Query(value = "select a.id from address a where a.state=:state", nativeQuery = true)
	public List<Integer> findByState(@Param("state") String state);
}
