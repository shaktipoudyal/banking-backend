package com.cerotid.bank.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cerotid.bank.model.Account;

@Repository
public interface AccountDao extends JpaRepository<Account, Integer> {

	@Query(value = "select * from account a where a.customer_id=:customer_id", nativeQuery = true)
	public List<Account> findByCustomerId(@Param("customer_id") int customer_id);

	@Query(value = "select bank_acc_number from account", nativeQuery = true)
	public List<Long> findExistingAccountNumbers();

	@Query(value = "select * from account a where a.bank_acc_number=:accountNumber", nativeQuery = true)
	public Account findByAccountNumber(@Param("accountNumber") long accountNumber);

}
