package com.cerotid.bank.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cerotid.bank.model.Customer;

@Repository
public interface CustomerDao extends JpaRepository<Customer, Integer> {

	// find customer name by state
	@Query(value = "select * from customer c inner join address a on c.address_id_fk=a.id where a.state_code=:state", nativeQuery = true)
	List<Customer> findCustomersByState(@Param("state") String state);

	// find customer by ssn
	@Query(value = "select * from customer c where c.ssn=:ssn", nativeQuery = true)
	public Customer findCustomerBySSN(@Param("ssn") String ssn);

	@Query(value = "select c.id, c.first_name, c.last_name, c.ssn from customer c", nativeQuery = true)
	List<Customer> findCustomerInfo();
}
