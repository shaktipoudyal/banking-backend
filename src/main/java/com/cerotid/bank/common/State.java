package com.cerotid.bank.common;

public enum State {

	AK, AL, AR, AZ, CA, CO, CT, DE, FL, GA, HI, IA, ID, IL, IN, KS, KY, LA, MA, MD, ME, MI, MN, MO, MS, MT, NC, ND, NE,
	NH, NJ, NM, NV, NY, OH, OK, OR, PA, RI, SC, SD, TN, TX, UT, VA, VT, WA, WI, WV, WY;

	State() {

	}

	private String state;

	State(String state) {
		this.state = state;
	}

	public String getState() {
		return state;
	}

}
