package com.cerotid.bank.common;

public final class Patterns {
	public static final String FIRST_NAME_PATTERN = "^[a-zA-Z\\s]*$";
	public static final String LAST_NAME_PATTERN = "^[a-zA-Z\\s]*$";
	public static final String SSN_PATTERN = "^[0-9]*$";

}
