package com.cerotid.bank.predicate;

import java.util.function.Predicate;

public class AccountPredicate {

	public static Predicate<Long> isDistinctAccountNumber(long number) {
		return p -> p == number;
	}
}