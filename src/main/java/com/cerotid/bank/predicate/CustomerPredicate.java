package com.cerotid.bank.predicate;

import java.util.function.Predicate;

import com.cerotid.bank.model.Customer;

public class CustomerPredicate {

	public static Predicate<Customer> isExistingSSN(String ssn) {
		return p -> p.getSsn().equals(ssn);
	}

}