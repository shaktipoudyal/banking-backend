package com.cerotid.bank.exceptions;

public enum CustomerExceptions {

	INVALID_FIRST_NAME("Invalid First Name. Name can only contain alphabets!"),
	INVALID_LAST_NAME("Invalid Last Name. Name can only contain alphabets!"),
	NAME_ALREADY_EXISTS("Customer name already exists!"), EMPTY_VALUES("First name and last name can not be empty"),
	CANNOT_FECTH_CUSTOMERS("Can not fetch customer details from database"),
	CANNOT_SAVE_CUSTOMER("Can not save customer information into database"), ILLEGAL_ID("Customer does not exist!"),
	INVALID_SSN("SSN must be 10 digit numbers only"),
	CUSTOMER_ALREADY_EXISTS("Duplicate ssn, customer already exists in system");

	private final String exception;

	CustomerExceptions(String exception) {
		this.exception = exception;
	}

	public String getException() {
		return this.exception;
	}

}
