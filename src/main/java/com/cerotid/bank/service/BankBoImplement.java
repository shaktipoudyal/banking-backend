package com.cerotid.bank.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cerotid.bank.common.Patterns;
import com.cerotid.bank.dao.AccountDao;
import com.cerotid.bank.dao.AddressDao;
import com.cerotid.bank.dao.CustomerDao;
import com.cerotid.bank.exceptions.CustomerExceptions;
import com.cerotid.bank.model.Account;
import com.cerotid.bank.model.AccountType;
import com.cerotid.bank.model.Customer;
import com.cerotid.bank.model.Transaction;
import com.cerotid.bank.predicate.AccountPredicate;
import com.cerotid.bank.predicate.CustomerPredicate;
import com.cerotid.bank.security.Decryptor;
import com.cerotid.bank.security.Encryptor;
import com.cerotid.bo.BankBO;

@Service
public class BankBoImplement implements BankBO {

	private static final Logger LOGGER = LoggerFactory.getLogger(BankBoImplement.class);

	@Autowired
	CustomerDao customerDao;

	@Autowired
	AddressDao addressDao;

	@Autowired
	AccountDao accountDao;

	public List<Customer> getCustomers() {
		List<Customer> list = new ArrayList<>();
		customerDao.findAll().forEach(list::add);

		for (int i = 0; i < list.size(); i++) {
			String ssn = list.get(i).getSsn();
			ssn = Decryptor.getDecryptedString(ssn);
			ssn = "*****" + ssn.substring(ssn.length() - 4, ssn.length());
			list.get(i).setSsn(ssn);
		}
		return list;
	}

	@Override
	public void addCustomer(Customer customer) throws Exception {
		LOGGER.info(" method: called with input: {}", customer.toString());

		System.out.println("service: " + customer.toString());

		String firstName = customer.getFirstName();
		String lastName = customer.getLastName();
		String ssn = customer.getSsn();

		validName(firstName, lastName);
		validSSN(ssn);

		String encryptedssn = Encryptor.getEncodedString(ssn);
		customer.setSsn(encryptedssn);

		List<Customer> customerList = new ArrayList<>();
		try {
			customerDao.findAll().forEach(customerList::add);
		} catch (Exception e) {
			LOGGER.error("error occured: {}", CustomerExceptions.CANNOT_FECTH_CUSTOMERS.getException());
			throw new Exception(CustomerExceptions.CANNOT_FECTH_CUSTOMERS.getException());
		}

		// using predicate-->
		for (Customer c : customerList) {
			if (CustomerPredicate.isExistingSSN(encryptedssn).test(c)) {
				LOGGER.error("error occured: {}", CustomerExceptions.CUSTOMER_ALREADY_EXISTS.getException());
				throw new Exception(CustomerExceptions.CUSTOMER_ALREADY_EXISTS.getException());
			}
		}

//		for (int i = 0; i < customer.getAccounts().size(); i++) {
//			AccountType accType = customer.getAccounts().get(i).getAccountType();
//			double amount = customer.getAccounts().get(i).getAmount();
//			customer.getAccounts().add(new Account(accType, amount));
//		}

		try {
			customerDao.save(customer);
			LOGGER.info(" method: successfully stored customer");
		} catch (Exception e) {
			LOGGER.error("error occured: {}", CustomerExceptions.CANNOT_SAVE_CUSTOMER.getException());
			throw new Exception(CustomerExceptions.CANNOT_SAVE_CUSTOMER.getException());
		}
	}

	@Override
	public void openAccount(Customer customer, Account account) {
		accountDao.save(account);
	}

	public void openCustomerAccount(Customer customer) {
		System.out.println(customer.toString());
		String firstName = customer.getFirstName();
		String lastName = customer.getLastName();
		String ssn = customer.getSsn();
		int customerId = customer.getId();

		System.out.println(firstName + ", " + lastName + ", " + ssn);
		System.out.println(customer.getAccounts().size());

		for (int i = 0; i < customer.getAccounts().size(); i++) {
			AccountType accType = customer.getAccounts().get(i).getAccountType();
			double amount = customer.getAccounts().get(i).getAmount();

			// generate acc numbner && check if it is distinct
			long accountNumber = 0;
			boolean isAccountNumberDistinct = false;
			while (isAccountNumberDistinct == false) {
				accountNumber = accountNumberGenerator();
				System.out.println(accountNumber);
				isAccountNumberDistinct = verifyDistinctAccountNumber(accountNumber);
			}

			System.out.println(accType + ", " + amount);
			Account account = new Account(accountNumber, accType, null, null, amount, customerId);
			accountDao.save(account);

		}
//		customerDao.save(customer);
	}

	public void addAccount(int customerId, Account account) {
		accountDao.save(account);
	}

	@Override
	public void sendMoney(Customer customer, Account account, Transaction transaction) {
		// TODO Auto-generated method stub

	}

	@Override
	public void depositMoneyInCustomerAccount(Customer customer) {
		// which account?
		// TODO Auto-generated method stub

	}

	public void deposit(long accountNumber, double amount) throws Exception {
		System.out.println("depositOrWithdraw called---");
		Account account = null;
		double currentAmount = 0;
		try {
			account = accountDao.findByAccountNumber(accountNumber);
		} catch (Exception e) {
			e.printStackTrace();
		}

		currentAmount = account.getAmount();
		if (currentAmount < 0) {
			if (amount > currentAmount) {
				throw new Exception("Withdraw amount is greater than available balance");
			}
		} else {
			if (amount > 100_000) {
				throw new Exception("Deposit amount is greater 100,000");
			}
		}

//		double result = currentAmount + amount;
//		System.out.println("Double: " + result);
//		account.setAmount(result);

		/*
		 * test test test
		 */
//		BigDecimal bg1 = new BigDecimal(currentAmount);
//		BigDecimal bg2 = new BigDecimal(amount);
//		BigDecimal bgResult = bg1.add(bg2);
//		System.out.println("Bigdecimal:" + bgResult);
//		System.out.println("Bigdecimal.doubleValue(): " + bgResult.doubleValue());
//		System.out.println();
//		BigDecimal bgg1 = new BigDecimal(String.valueOf(currentAmount));
//		BigDecimal bgg2 = new BigDecimal(String.valueOf(amount));
//		BigDecimal bggResult = bgg1.add(bgg2);
//		System.out.println("Bigdecimal:" + bggResult);
//		System.out.println("Bigdecimal.doubleValue(): " + bggResult.doubleValue());

		BigDecimal currentAmtBgDecimal = new BigDecimal(String.valueOf(currentAmount));
		BigDecimal additionalAmtBgDecimal = new BigDecimal(String.valueOf(amount));
		BigDecimal resultBgDecimal = currentAmtBgDecimal.add(additionalAmtBgDecimal);
		account.setAmount(resultBgDecimal.doubleValue());

		try {
			accountDao.save(account);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void withdraw(long accountNumber, double amount) throws Exception {
		System.out.println("withdraw called---");
		Account account = null;
		double currentAmount = 0;
		try {
			account = accountDao.findByAccountNumber(accountNumber);
		} catch (Exception e) {
			e.printStackTrace();
		}

		currentAmount = account.getAmount();

		if (amount > currentAmount) {
			throw new Exception("Withdraw amount is greater than available balance");
		}

		BigDecimal currentAmtBgDecimal = new BigDecimal(String.valueOf(currentAmount));
		BigDecimal withdrawAmtBgDecimal = new BigDecimal(String.valueOf(amount));
		BigDecimal resultBgDecimal = currentAmtBgDecimal.subtract(withdrawAmtBgDecimal);

		account.setAmount(resultBgDecimal.doubleValue());

		try {
			accountDao.save(account);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void editCustomerInfo(Customer customer) throws Exception {
		System.out.println("customer.getCustomerId(): " + customer.getId());
		LOGGER.info(" method: called with input: {}", customer.toString());

		if (customerDao.existsById(customer.getId()) == false) {
			throw new Exception(CustomerExceptions.ILLEGAL_ID.getException());
		}

		String firstName = customer.getFirstName();
		String lastName = customer.getLastName();
		String ssn = customer.getSsn();

		validName(firstName, lastName);
		validSSN(ssn);

		String encryptedssn = Encryptor.getEncodedString(ssn);
		customer.setSsn(encryptedssn);

		// get all exiting customer list
		List<Customer> customerList = new ArrayList<>();
		try {
			customerDao.findAll().forEach(customerList::add);
		} catch (Exception e) {
			LOGGER.error("error occured: {}", CustomerExceptions.CANNOT_FECTH_CUSTOMERS.getException());
			throw new Exception(CustomerExceptions.CANNOT_FECTH_CUSTOMERS.getException());
		}

		// check if ssn matches with other existing customer
		for (Customer c : customerList) {
			if (c.getId() == customer.getId()) {
				continue;
			}
			if (CustomerPredicate.isExistingSSN(encryptedssn).test(c)) {
				LOGGER.error("error occured: {}", CustomerExceptions.CUSTOMER_ALREADY_EXISTS.getException());
				throw new Exception(CustomerExceptions.CUSTOMER_ALREADY_EXISTS.getException());
			}
		}

		try {
			customerDao.save(customer);
			LOGGER.info(" method: successfully updated customer");
		} catch (Exception e) {
			LOGGER.error("error occured: {}", CustomerExceptions.CANNOT_SAVE_CUSTOMER.getException());
			throw new Exception(CustomerExceptions.CANNOT_SAVE_CUSTOMER.getException());
		}
	}

	public void deleteCustomer(int id) throws Exception {
		LOGGER.info(" method: called with customer id: {}", id);

		if (customerDao.existsById(id)) {
			try {
				customerDao.deleteById(id);
			} catch (Exception e) {
				throw new Exception("Exception occured");
			}
		}

		// also delete accounts for the customer
		List<Account> customerAccounts = new ArrayList<>();
		accountDao.findByCustomerId(id).forEach(customerAccounts::add);
		System.out.println("customerAccounts length" + customerAccounts.size());

		Iterator<Account> itrr = customerAccounts.iterator();
		while (itrr.hasNext()) {
			int customerId = itrr.next().getId();
			accountDao.deleteById(customerId);
		}
		System.out.println("customerAccounts length" + customerAccounts.size());
	}

	@Override
	public Customer getCustomerInfo(String ssn) throws Exception {
		LOGGER.info(" method: called with input: {}", ssn);

		Customer c = null;
		String encryptedssn = Encryptor.getEncodedString(ssn);
		try {
			c = customerDao.findCustomerBySSN(encryptedssn);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("error occured: {}", CustomerExceptions.CANNOT_FECTH_CUSTOMERS.getException());
			throw new Exception(CustomerExceptions.CANNOT_FECTH_CUSTOMERS.getException());
		}
		int customerId = c.getId();

		String decryptedssn = Decryptor.getDecryptedString(c.getSsn());
		decryptedssn = "*****" + decryptedssn.substring(decryptedssn.length() - 4, decryptedssn.length());
		c.setSsn(decryptedssn);

		List<Account> customerAccounts = new ArrayList<>();
		accountDao.findByCustomerId(customerId).forEach(customerAccounts::add);

		c.setAccounts(customerAccounts);

		System.out.println("Returning: " + c.toString());
		return c;
	}

	@Override
	public void printBankStatus() {
		// TODO Auto-generated method stub

	}

	@Override
	public void serializeBank() {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Customer> getCustomersByState(String stateCode) throws Exception {
		List<Customer> customers = new ArrayList<>();
		try {
			customerDao.findCustomersByState(stateCode).forEach(customers::add);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(CustomerExceptions.CANNOT_FECTH_CUSTOMERS.getException());
		}
		return customers;
	}

	/*
	 * utility methods-->
	 */
	private void validName(String firstName, String lastName) throws Exception {
		if (firstName.length() == 0 || lastName.length() == 0) {
			throw new Exception(CustomerExceptions.EMPTY_VALUES.getException());
		} else if (!firstName.matches(Patterns.FIRST_NAME_PATTERN)) {
			throw new Exception(CustomerExceptions.INVALID_FIRST_NAME.getException());
		} else if (!lastName.matches(Patterns.LAST_NAME_PATTERN)) {
			throw new Exception(CustomerExceptions.INVALID_LAST_NAME.getException());
		}
	}

	private void validSSN(String ssn) throws Exception {
		if (ssn.length() == 0 || !ssn.matches(Patterns.SSN_PATTERN)) {
			throw new Exception(CustomerExceptions.INVALID_SSN.getException());
		}
	}

	/*
	 * for test only
	 */
	public List<Account> getAccounts() {
		List<Account> list = new ArrayList<>();
		accountDao.findAll().forEach(list::add);
		return list;
	}

	public Account findByAccountNumber(long accountNumber) {
		Account account = null;
		try {
			account = accountDao.findByAccountNumber(accountNumber);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return account;
	}

	public long accountNumberGenerator() {
		long number = (long) Math.floor(Math.random() * 9_000_000_000L) + 1_000_000_000L;
		return number;
	}

	public boolean verifyDistinctAccountNumber(long number) {
		System.out.println("verifyDistinceAccountNumber called" + number);
		List<Long> existingAccountNumbers = new ArrayList<>();
		accountDao.findExistingAccountNumbers().forEach(existingAccountNumbers::add);

		Predicate<Long> p = AccountPredicate.isDistinctAccountNumber(number);
		for (Long l : existingAccountNumbers) {
			if (p.test(l)) {
				return false;
			}
		}
		return true;
	}

}
