package com.cerotid.bank;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.event.EventListener;

@SpringBootApplication
@ComponentScan({ "com.cerotid.bank.*", "com.cerotid.bank" })
//@EntityScan("com.cerotid.bank.*")
public class CerotidbankingApplication {
	static final Logger LOGGER = LoggerFactory.getLogger(CerotidbankingApplication.class);

	// Setting Spring Boot SetTimeZone
//	@PostConstruct
//	public void init() {
//		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
//
//	}

	@Value("${server.port}")
	private int port;

	public static void main(String[] args) {

		SpringApplication.run(CerotidbankingApplication.class, args);

	}

	@EventListener(ApplicationReadyEvent.class)
	public void onAppStart() {
		System.out.println(String.format("http://localhost:%d", this.port));
	}
}