package com.cerotid.bank.springsecurity;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.cerotid.bank.dao.UserDao;
import com.cerotid.bank.service.BankBoImplement;

@Service
public class MyUserDetailsService implements UserDetailsService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BankBoImplement.class);

	@Autowired
	UserDao userDao;

	@Autowired
	JwtRequestFilter jwtFilter;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		LOGGER.info(" method: called with input: {}", username.toString());

		com.cerotid.bank.model.User user = userDao.findByUserName(username);
		return new User(user.getUserName(), user.getPassword(), new ArrayList<>());
	}

	public int register(com.cerotid.bank.model.User user) throws Exception {
		LOGGER.info(" method: called with input: {}", user.toString());

		String userName = null;
		com.cerotid.bank.model.User existingUser = null;
		if (user != null) {
			userName = user.getUserName();
			existingUser = userDao.findByUserName(userName);
		}

		if (existingUser != null) {
			throw new Exception("Username already exists!");
		}

		if (user.getPassword() == null) {
			System.out.println("return 200---");
			return 200;
		} else {
			return userDao.save(user).getId();
		}
	}

	public void usernameAvailability(com.cerotid.bank.model.User user) throws Exception {
		LOGGER.info(" method: called with input: {}", user.toString());
		String userName = user.getUserName();
		com.cerotid.bank.model.User existingUser = userDao.findByUserName(userName);

		if (existingUser.getUserName().equalsIgnoreCase(userName)) {
			throw new Exception("Username already exists!");
		}

	}

	public void logout() throws Exception {
		LOGGER.info(" method: called");
		jwtFilter.invalidateJwtToken();
	}
}