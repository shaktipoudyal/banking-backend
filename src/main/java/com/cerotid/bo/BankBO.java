package com.cerotid.bo;

import java.util.List;

import com.cerotid.bank.model.Account;
import com.cerotid.bank.model.Customer;
import com.cerotid.bank.model.Transaction;

public interface BankBO {

	public void addCustomer(Customer customer) throws Exception;

	public void openAccount(Customer customer, Account account);

	public void sendMoney(Customer customer, Account account, Transaction transaction);

	public void depositMoneyInCustomerAccount(Customer customer);

	public void editCustomerInfo(Customer customer) throws Exception;

	public Customer getCustomerInfo(String ssn) throws Exception;

	public void printBankStatus();

	public void serializeBank();

	public List<Customer> getCustomersByState(String StateCode) throws Exception;

}
