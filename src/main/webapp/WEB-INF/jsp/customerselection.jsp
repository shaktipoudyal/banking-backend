<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div>
		<form:form action="openAccount" method="post"
			modelAttribute="customer">
			<form:label path="accounts">Select Customer: </form:label>
			<form:select path="accounts">
				<form:option value="" label="Select customer: " />
				<form:options items="${accounts}" />
			</form:select>
		</form:form>
	</div>
	<br />
	<div class="mainmenu">
		<a href="/account">Open Account</a>
	</div>
</body>
</html>