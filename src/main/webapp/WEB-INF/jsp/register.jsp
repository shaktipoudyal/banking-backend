<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link href="/resources/register.css" rel="stylesheet" type="text/css">
</head>
<body>

	<%-- <%
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");

		if (firstName == null || "".equals(firstName) || lastName == null || "".equals(lastName)) {
			throw new ServletException("Mandatory Parameter missing");
		}
	%> --%>


	<h2>Customer Register Form</h2>
	<%-- <form:form action="register" method="post" modelAttribute="customerReg"> --%>

	<form:form action="register" method="post" modelAttribute="customer">
		<div class="container">
			<form:label path="firstName">First Name: </form:label>
			<form:input path="firstName" />
			<br />
			<form:label path="lastName">Last Name: </form:label>
			<form:input path="lastName" />
			<br />
			<form:label path="ssn">SSN: </form:label>
			<form:input path="ssn" />
			<br />
			<form:label path="address.streetName">Street Code:</form:label>
			<form:input path="address.streetName" />
			<br />
			<form:label path="address.city">City:</form:label>
			<form:input path="address.city" />
			<br />
			<%-- <form:label path="address.stateCode">State:</form:label>
			<form:input path="address.stateCode" /> --%>
			<form:label path="address.zipCode">Zip Code:</form:label>
			<form:input path="address.zipCode" />
			<br />
			<%-- <form:select path="address.stateCode" id="state">
				<c:forEach var="states" items="${state}">
					<form:option value="${states}">${states}
					 <c:out value="${option.states}"></c:out>
					</form:option>
				</c:forEach>
				<form:option value="${stateCode}">${states}</form:option>
			</form:select> --%>

			<form:label path="address.stateCode">State:</form:label>
			<form:select path="address.stateCode">
				<form:option value="NONE" label="Select your state code" />
				<form:options items="${stateCode}" />
			</form:select>

		</div>
		<br />
		<form:button class="button">Register</form:button>
		<!--  <input class="button" name="Register" value="Register" type="submit" disabled/> -->
	</form:form>
	<div class="mainmenu">
		<a href="/">Main Menu</a>
	</div>

</body>
</html>