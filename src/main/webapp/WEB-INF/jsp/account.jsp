<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link href="/resources/register.css" rel="stylesheet" type="text/css">
</head>
<body>

	<h2>Add Account</h2>

	<form:form action="openAccount" method="post" modelAttribute="customer">
		<div class="container">
			Customer Name:
			<br /> <br />
			<%-- <form:label path="accounts.accountType">Account:</form:label>
			<form:select path="accounts.accountType">
				<form:option value="" label="Select Account Type" />
				<form:options items="${accountType}" />
			</form:select> --%>
			
			<%-- <form:label path="">Account Type:</form:label>
			<form:select path="">
				<form:option value="" label="Select Account Type" />
				<form:options items="${accountType}" />
			</form:select> --%>
			
			<form:label path="">Account Type:</form:label>
			<form action="main.jsp" method="POST" target="_blank">
				<input type="checkbox" name="maths" checked="checked" /> CHECKING
				<input type="checkbox" name="physics" /> SAVINGS
				<input type="checkbox" name="chemistry" /> BUSINESS 
				<!-- <input type="submit" value="Select Subject" /> -->
			</form>
			
			<br /> <br />
			<div>
				<form:label path="">Initial Balance: </form:label>
				<input type="number" name="initialBalance" />
			</div>
			<br /> <br />
		</div>
		<br />
		<form:button class="button">Register</form:button>
	</form:form>

</body>
</html>